package br.com.itau.cliente.controllers;

import br.com.itau.cliente.dtos.ClienteMapper;
import br.com.itau.cliente.dtos.CriarClienteDTO;
import br.com.itau.cliente.models.Cliente;
import br.com.itau.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper clienteMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente criarCliente(@RequestBody @Valid CriarClienteDTO clienteDTO) {
        try {
            Cliente cliente = clienteMapper.toCliente(clienteDTO);
            return clienteService.criarCliente(cliente);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Cliente consultarClientePorId(@PathVariable(name = "id") int id) {
        return clienteService.consultarClientePorId(id);
    }
}
