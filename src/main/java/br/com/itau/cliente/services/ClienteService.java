package br.com.itau.cliente.services;

import br.com.itau.cliente.exceptions.ClienteNotFoundException;
import br.com.itau.cliente.models.Cliente;
import br.com.itau.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente) {
        try {
            return clienteRepository.save(cliente);
        } catch (DataIntegrityViolationException e) {
            throw new RuntimeException("Cliente já está cadastrado no sistema.");
        }
    }

    public Cliente consultarClientePorId(int id) {
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);
        if(!optionalCliente.isPresent()) {
            throw new ClienteNotFoundException();
        }
        return optionalCliente.get();
    }
}
